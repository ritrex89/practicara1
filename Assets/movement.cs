using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
        private Touch touch;
        private float speed;
    private Vector3 startPos;
    GameObject table;

    // Start is called before the first frame update
    void Awake() {
        speed = 0.0005f;
        table = GameObject.Find("Table");
        startPos = table.GetComponent<Transform>().position;
    }
    void Start()
    {
        
    }

    void FixedUpdate() {
        startPos = table.GetComponent<Transform>().position;
        Debug.Log(transform.position);
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(
                    transform.position.x + (touch.deltaPosition.x * speed),
                    transform.position.y,
                    transform.position.z + (touch.deltaPosition.y * speed)
                    );
            }
        }
        if (Vector3.Distance(transform.position,startPos) > 0.13f)
            transform.position = startPos + new Vector3(0, 0.05f,0) ;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
