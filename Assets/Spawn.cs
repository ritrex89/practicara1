using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Spawn : MonoBehaviour
{
    int count;
    float min, max, scale;
    Vector3 startPos;
    float massDelta;
    GameObject table;
    GameObject plate;
    Vector3 platePos;
    public int plateCatch = 0;
    GameObject score;
    void Awake() {
        count = 0;
        min = -8.0f;
        max = 8.0f;
        scale = 0.01f;
        
        table = GameObject.Find("Table");
        plate = GameObject.Find("spawn");
        score = GameObject.Find("Score");
        plateCatch = 0;
        startPos = table.GetComponent <Transform> ().position;
        platePos = plate.GetComponent<Transform>().position;
        //GetComponent<Rigidbody>().AddForce(new Vector3(0f, 1 - massDelta,0f),ForceMode.Force);

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        startPos = table.GetComponent<Transform>().position;
        
        count = count + 1;

        if (transform.position.y<startPos.y || isCaught())
        {
            count = 0;
            if (isCaught())
            {
                plateCatch = plateCatch + 1;
                score.GetComponent<Text>().text = "Score: " + plateCatch;
            }
            float xv = startPos.x + Random.Range(min*scale,max*scale);
            float zv = startPos.z + Random.Range(min*scale,max*scale);
            transform.position = new Vector3(xv,startPos.y+0.5f,zv);
            //Debug.Log("Brocoli: ");
            //Debug.Log(transform.position);
        }
        transform.position = new Vector3(transform.position.x, transform.position.y-0.002f, transform.position.z);
        //Debug.Log("Brocoli: ");
        //Debug.Log(transform.position);
        Debug.Log(plateCatch);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool isCaught()
    {
        platePos = plate.GetComponent<Transform>().position;
        return (Vector3.Distance(platePos,transform.position)<0.025);
    }

    void changeScore() {
        plateCatch = plateCatch + 1;
    }
}
